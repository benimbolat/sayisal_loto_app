﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Random_ile_Sayısal_Loto
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.Yellow;

        }

        private void kırmızıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Red;
        }

        private void maviToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Blue;

        }

        private void yeşilToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Green;
        }

        private void sarıToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Yellow;
        }

        private void turuncuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Orange;
        }

        private void suYeşiliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.SeaGreen;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rstgl = new Random();
            int s1, s2, s3, s4;

            s1 = rstgl.Next(1, 9);
            s2 = rstgl.Next(1, 9);
            s3 = rstgl.Next(1, 9);
            s4 = rstgl.Next(1, 9);

            //Yazdırma komutları buraya ->
            textBox1.Text = s1.ToString();
            textBox2.Text = s2.ToString();
            textBox3.Text = s3.ToString();
            textBox4.Text = s4.ToString();



        }

        private void turuncuToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void kırmızıToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Red;

        }

        private void sarıToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Yellow;
            linkLabel1.LinkColor = Color.Black;
        }

        private void maviToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Blue;

        }

        private void yeşilToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.BackColor = Color.Green;

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Application.Exit();

        }
    }
}
